import { StarCanvas } from "./modules/canvas/StarCanvas.mjs";
import { MouseCanvas } from "./modules/canvas/MouseCanvas.mjs";

window.addEventListener("load", function () {
  if (window.addEventListener) {
    window.addEventListener("resize", Canvas_initAll, true);
  } else if (window.attachEvent) {
    window.attachEvent("onresize", Canvas_initAll);
  }
  Canvas_initAll();
});

//Gestion des canvas
var canvasFond;
var canvasMouse;
var IdAnimationFrameCanvas;
function Canvas_initAll() {
  if (document.getElementById("fond")) {
    canvasFond = new StarCanvas("fond");
    canvasFond.init();
  } else {
    canvasFond = false;
  }
  if (
    document.getElementById("mouse") &&
    window.matchMedia("(min-width: 769px)").matches &&
    window.matchMedia("(min-height: 768px)").matches
  ) {
    canvasMouse = new MouseCanvas("mouse", parseInt(Math.random() * 10) + 2);
    canvasMouse.init();
    Canvas_eventMouse();
  } else {
    canvasMouse = false;
  }
  cancelAnimationFrame(IdAnimationFrameCanvas);
  IdAnimationFrameCanvas = requestAnimationFrame(Canvas_animateAll);
}
function Canvas_eventMouse() {
  //-on met en place les évenements
  var ListItemAvecVignette = document.querySelectorAll(
    ".tabList-item > .tabList-item-vignette"
  );
  ListItemAvecVignette.forEach((LIAV_item) => {
    var LIAV_parent = LIAV_item.parentElement;
    LIAV_parent.addEventListener("mouseout", () => {
      canvasMouse.element.style.opacity = 1;
    });
    LIAV_parent.addEventListener("mouseover", (event) => {
      canvasMouse.element.style.opacity = 0;
    });
  });

  var detectCursors = document.querySelectorAll("a, button, .detectCursor");
  detectCursors.forEach((detectCursor) => {
    detectCursor.addEventListener("mouseenter", (event) => {
      canvasMouse.element.style.filter = "invert(1)";
    });
    detectCursor.addEventListener("mouseleave", (event) => {
      canvasMouse.element.style.filter = "invert(0)";
    });
  });
}
function Canvas_animateAll() {
  if (canvasFond || canvasMouse) {
    IdAnimationFrameCanvas = requestAnimationFrame(Canvas_animateAll);
  }
  if (canvasMouse) {
    canvasMouse.animate();
  }
  if (canvasFond) {
    canvasFond.animate();
  }
}
