import { ScrollHoriz } from "./modules/ScrollActions/ScrollHoriz.mjs";
import { ScrollHeader } from "./modules/ScrollActions/ScrollHeader.mjs";

window.addEventListener("load", function () {
  initSrcollActions();
});

//Gestion des Scrolls

function initSrcollActions() {
  var s_header = new ScrollHeader(".header", ".hero");
  var s_horiz = new ScrollHoriz(".horiz", ".horizContent", 200);
  s_header.initHeaderScroll();
  s_horiz.initHeaderScroll();
}
