export class Canvas {
  constructor(id) {
    this.id = id;
    this.element = document.getElementById(id);
    this.context = this.element.getContext("2d");
    this.width = this.element.clientWidth;
    this.height = this.element.clientHeight;
    this.element.height = this.height;
    this.element.width = this.width;
  }
}
