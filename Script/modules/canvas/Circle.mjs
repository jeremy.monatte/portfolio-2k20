export class Circle {
  constructor(x, y, velocityX, velocityY, radius) {
    this.x = x;
    this.y = y;
    this.velocityX = velocityX;
    this.velocityY = velocityY;
    this.radius = radius;
    this.color = "#fff";
  }
  draw(context2D) {
    context2D.beginPath();
    context2D.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
    context2D.fillStyle = this.color;
    context2D.fill();
  }
  update(context2D, canvasH,canvasW) {
    if (this.x + this.radius > canvasW || this.x - this.radius < 0) {
      this.velocityX = -this.velocityX;
    }
    if (this.y + this.radius > canvasH|| this.y - this.radius < 0) {
      this.velocityY = -this.velocityY;
    }
    this.x += this.velocityX;
    this.y += this.velocityY;
    this.draw(context2D);
  }
}
