import { Point } from "./Point.mjs";
import { Canvas } from "./Canvas.mjs";

export class MouseCanvas extends Canvas {
  constructor(id, nbPoint = 3, mouseRadius = 10) {
    super(id);
    this.posArray = [];
    this.nbPoint = nbPoint;
    this.mouseRadius = mouseRadius;
    this.mousePos = {
      x: undefined,
      y: undefined,
    };
    this.pointer = false;
    this.hide = false;
  }
  init() {
    if (window.matchMedia("(any-pointer)").matches) {
      //Si il y a un pointer :
      //-on met en place les données initialles
      for (var i = 0; i < this.nbPoint; i++) {
        this.posArray.push(new Point(this.width / 2, this.height / 2, i));
      }
      window.addEventListener("mousemove", (event) => {
        this.mousePos.x = event.x;
        this.mousePos.y = event.y;
      });
      //-on suprimme l'autre curseur
      //$("body *").css("cursor", "none");
      //-on commence les tracés
      this.animate();
    }
  }

  animate() {
    this.context.clearRect(0, 0, this.width, this.height);
    for (var i = this.posArray.length - 1; i >= 0; i--) {
      this.posArray = this.posArray[i].update(this.posArray, this.mousePos);
    }
    this.context.strokeStyle = "#ffffff";
    this.context.beginPath();
    this.context.arc(
      this.posArray[0].x,
      this.posArray[0].y,
      10,
      Math.PI * 2,
      false
    );
    this.context.lineWidth = 1;
    this.context.stroke();
    this.context.fillStyle = "#ffffff";
    this.context.beginPath();
    this.context.moveTo(this.posArray[0].x, this.posArray[0].y);
    for (var j = 0; j < this.posArray.length; j++) {
      this.posArray[j].drawLine(this.context);
    }
    this.context.lineWidth = this.mouseRadius * (3 / 4);
    this.context.lineCap = "round";
    this.context.stroke();
  }
}
