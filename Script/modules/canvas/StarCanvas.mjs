import { Circle } from "./Circle.mjs";
import { Canvas } from "./Canvas.mjs";

export class StarCanvas extends Canvas {
  constructor(id, nbEtoile = (window.innerWidth * window.innerHeight) / 20000) {
    super(id);
    this.nbEtoile = nbEtoile;
    this.etoileArray = [];
  }
  init() {
    for (var i = 0; i < this.nbEtoile; i++) {
      let radius = Math.random() * 3;
      let x = Math.random() * (this.width - radius * 2) + radius;
      let y = Math.random() * (this.height - radius * 2) + radius;
      let velocityX = (Math.random() - 0.5) * 5;
      let velocityY = (Math.random() - 0.5) * 5;
      this.etoileArray.push(new Circle(x, y, velocityX, velocityY, radius));
    }
  }
  animate(canvas) {
    this.context.clearRect(0, 0, this.width, this.height);
    for (var i = 0; i < this.etoileArray.length; i++) {
      this.etoileArray[i].update(this.context, this.height, this.width);
    }
  }
}
