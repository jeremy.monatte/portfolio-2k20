export class Point {
  constructor(x, y, num) {
    this.x = x;
    this.y = y;
    this.num = num;
    this.color = "red";
  }
  drawLine(context2D) {
    context2D.lineTo(this.x, this.y);
  }
  update(posArray, mousePos) {
    if (this.num != 0 && this.num != posArray.length - 1) {
      this.x = posArray[this.num - 1].x;
      this.y = posArray[this.num - 1].y;
    } else if (this.num == posArray.length - 1) {
      this.x = posArray[this.num - 1].x;
      this.y = posArray[this.num - 1].y;
    } else if (this.num == 0) {
      this.x = mousePos.x;
      this.y = mousePos.y;
    }
    return posArray;
  }
}
