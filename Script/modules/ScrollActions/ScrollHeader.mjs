export class ScrollHeader {
  constructor(headerSelector = ".header", heroSelector = ".hero") {
    this.headerSelector = headerSelector;
    this.heroSelector = heroSelector;
    this.header = document.querySelector(this.headerSelector);
    this.hero = document.querySelector(this.heroSelector);
  }
  initHeaderScroll() {
    if (this.header && this.hero) {
      document.addEventListener("scroll", () => {
        this.headerScroll();
      });
    } else {
      console.warn("Vous n'avez pas défini le header, ou il n'exxiste pas");
    }
  }
  headerScroll() {
    var screenH = window.innerHeight;
    var ratio = 1 - this.header.getBoundingClientRect().top / screenH;
    if (ratio < 2) {
      this.hero.style.transform =
        "scale(" + ratio + ") rotate(" + (1 - ratio) * 90 + "deg)";
    }
  }
}
