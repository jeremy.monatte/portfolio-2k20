export class ScrollHoriz {
  constructor(sectSelector = false, contSelector = false, amortisseur = 200) {
    this.sect = document.querySelector(sectSelector);
    this.cont = document.querySelector(contSelector);
    this.amortisseur = amortisseur;
  }
  initHeaderScroll() {
    if (this.sect && this.cont) {
      document.addEventListener("scroll", () => {
        this.horizScroll();
      });
    } else {
      console.warn("Vous n'avez pas défini l'element, ou il n'existe pas");
    }
  }
  horizScroll() {
    console.log(
      window.matchMedia("(min-width: 769px)").matches,
      window.matchMedia("(min-height: 768px)").matches
    );
    if (
      window.matchMedia("(min-width: 769px)").matches &&
      window.matchMedia("(min-height: 768px)").matches
    ) {
      var ratio = (this.cont.offsetHeight * 100) / this.sect.offsetHeight;
      var scrolled =
        (-this.sect.getBoundingClientRect().top * 100) / this.sect.offsetHeight;
      var scrolledAmorti =
        (-(this.sect.getBoundingClientRect().top + this.amortisseur) * 100) /
        (this.sect.offsetHeight - this.amortisseur * 3);

      if (scrolled < 0) {
        this.cont.style.top = 0;
      } else if (scrolled > 100 - ratio) {
        this.cont.style.top = 100 - ratio + "%";
      } else {
        this.cont.style.top = scrolled + "%";
      }

      if (scrolledAmorti < 0) {
        this.cont.style.left = 0;
      } else if (scrolledAmorti > 100 - ratio) {
        this.cont.style.left = -100 + "vw";
      } else {
        this.cont.style.left = -(scrolledAmorti / (100 - ratio)) * 100 + "vw";
      }
    } else {
      this.cont.style.top = 0;
      this.cont.style.left = 0;
    }
  }
}
