window.addEventListener("load", function () {
  $(".loader").addClass("loader--hide");
  $(".tabList-part-header").click(togggleListPart);
  $(".tabList-item-vignette").parent(".tabList-item").mousemove(vignetteProjet);
  $(".indicateur").parents(".tabList-item").mouseover(toggleIndicateur);
});

function togggleListPart() {
  var listPart = $(this).parent(".tabList-part");
  listPart.siblings().removeAttr("open");
  if (listPart.attr("open")) {
    listPart.removeAttr("open");
  } else {
    listPart.attr("open", true);
  }
}

function toggleIndicateur() {
  if (!$(this).find(".indicateur").attr("open")) {
    $(this).find(".indicateur").attr("open", true);
  }
}
function vignetteProjet(event) {
  $(this)
    .find(".tabList-item-vignette")
    .css({ top: event.offsetY, left: event.offsetX });
}
