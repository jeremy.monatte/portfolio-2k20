window.addEventListener("load", function () {
  setupEvents();
  var url = document.URL;
  var hash = url.substring(url.indexOf("#") + 1);
  if (hash) {
    $(".world").addClass("fast");
    $("button.link--navigation[data-target=" + hash + "]").click();
    setInterval(() => {
      $(".world").removeClass("fast");
    }, 10);
  }
});
function setupEvents() {
  $("button.link--navigation[data-target]").click(cliCKOnLinkToSelectWorldPart);
  $("#sectionSelect").change(moveInWorld);
  console.log("kjhb");
}

function cliCKOnLinkToSelectWorldPart(event) {
  event.preventDefault;
  var wasActive = false;
  if ($(this).hasClass("active")) {
    document.querySelector("#sectionSelect").selectedIndex = 0;
    wasActive = true;
  } else {
    var dataTarget = $(this).attr("data-target");
    var options = document.querySelector("#sectionSelect").options;
    var optionsValue = [];
    for (let i = 0; i < options.length; i++) {
      optionsValue.push(options[i].value);
    }
    var indexOfDataTarget = optionsValue.indexOf(dataTarget);
    if (indexOfDataTarget != -1) {
      document.querySelector(
        "#sectionSelect"
      ).selectedIndex = indexOfDataTarget;
    } else {
      document.querySelector("#sectionSelect").selectedIndex = 0;
    }
  }
  $(".active").toggleClass("active");
  if (!wasActive) {
    $(this).toggleClass("active");
  }
  $("#sectionSelect").change();
}
function moveInWorld() {
  switch (this.value) {
    case "home":
      document
        .querySelector(".world")
        .classList.remove("world--avant", "world--gauche", "world--droite");
      break;
    case "projet":
      document
        .querySelector(".world")
        .classList.remove("world--gauche", "world--droite");
      document.querySelector(".world").classList.add("world--avant");
      document.querySelector(".section--avant").classList.remove("notApear");

      break;
    case "about":
      document
        .querySelector(".world")
        .classList.remove("world--avant", "world--droite");
      document.querySelector(".world").classList.add("world--gauche");
      document.querySelector(".section--gauche").classList.remove("notApear");

      break;
    case "contact":
      document
        .querySelector(".world")
        .classList.remove("world--gauche", "world--avant");
      document.querySelector(".world").classList.add("world--droite");
      document.querySelector(".section--droite").classList.remove("notApear");
      break;
    default:
      break;
  }
}
